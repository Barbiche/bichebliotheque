﻿using System;
using System.Collections.Generic;
using _2DShapes;
using AutoFixture.Xunit2;
using BarbFoundation;
using Xunit;

namespace _2DShapes_UT
{
    public class RectangleCalculationExtensionsTests
    {
        [Theory]
        [AutoData]
        public void UnionTest(Rectangle sut, Point2I p)
        {
            var result = sut.Union(p);

            // Assert
            Assert.Equal(Math.Min(sut.Min.X, p.X), result.Min.X);
            Assert.Equal(Math.Min(sut.Min.Y, p.Y), result.Min.Y);
            Assert.Equal(Math.Max(sut.Max.X, p.X), result.Max.X);
            Assert.Equal(Math.Max(sut.Max.Y, p.Y), result.Max.Y);
        }

        [Theory]
        [AutoData]
        public void UnionRectangleTest(Rectangle sut, Rectangle r)
        {
            var result = sut.Union(r);

            // Assert
            Assert.Equal(Math.Min(sut.Min.X, r.Min.X), result.Min.X);
            Assert.Equal(Math.Min(sut.Min.Y, r.Min.Y), result.Min.Y);
            Assert.Equal(Math.Max(sut.Max.X, r.Max.X), result.Max.X);
            Assert.Equal(Math.Max(sut.Max.Y, r.Max.Y), result.Max.Y);
        }

        public static IEnumerable<object[]> IntersectRectangles()
        {
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(3, 2), new Point2I(4, 3)),
                new Rectangle(new Point2I(3, 2), new Point2I(3, 2))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(2, 1), new Point2I(4, 3)),
                new Rectangle(new Point2I(2, 1), new Point2I(3, 2))
            };
        }


        [Theory]
        [MemberData(nameof(IntersectRectangles))]
        public void Intersect(Rectangle sut, Rectangle r, Rectangle result)
        {
            Assert.Equal(result, sut.Intersect(r));
        }

        public static IEnumerable<object[]> DistinctRectangles()
        {
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(3, 0), new Point2I(6, 2))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 2), new Point2I(3, 4))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(-3, 0), new Point2I(0, 2))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, -2), new Point2I(2, 0))
            };
        }

        public static IEnumerable<object[]> DistinctRectanglePoint()
        {
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(6, 2)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(3, 4)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(0, 2)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(2, 0)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(3, 0)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(0, 2)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(-3, 0)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(0, -2)
            };
        }

        public static IEnumerable<object[]> InsideRectanglePoint()
        {
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(1, 1)
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Point2I(2, 1)
            };
        }

        public static IEnumerable<object[]> OverlappingRectangles()
        {
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(2, 0), new Point2I(5, 2))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 1), new Point2I(3, 3))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(-2, 0), new Point2I(1, 2))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, -1), new Point2I(2, 1))
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 0), new Point2I(4, 2)),
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 0), new Point2I(3, 3)),
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(-1, 0), new Point2I(3, 2)),
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 1), new Point2I(3, 2)),
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(-1, -1), new Point2I(4, 3)),
            };
            yield return new object[]
            {
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
                new Rectangle(new Point2I(0, 0), new Point2I(3, 2)),
            };
        }

        [Theory]
        [MemberData(nameof(DistinctRectangles))]
        public void OverlapsOut(Rectangle sut, Rectangle r)
        {
            Assert.False(sut.Overlaps(r));
        }

        [Theory]
        [MemberData(nameof(OverlappingRectangles))]
        public void OverlapsIn(Rectangle sut, Rectangle r)
        {
            Assert.True(sut.Overlaps(r));
        }

        [Theory]
        [MemberData(nameof(DistinctRectanglePoint))]
        public void InsideOut(Rectangle sut, Point2I p)
        {
            Assert.False(p.IsInside(sut));
        }

        [Theory]
        [MemberData(nameof(InsideRectanglePoint))]
        public void InsideIn(Rectangle sut, Point2I p)
        {
            Assert.True(p.IsInside(sut));
        }
    }
}