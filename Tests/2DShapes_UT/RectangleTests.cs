using System;
using _2DShapes;
using AutoFixture.Xunit2;
using BarbFoundation;
using Xunit;

namespace _2DShapes_UT
{
    public class RectangleTests
    {
        [Theory]
        [AutoData]
        public void CreateTest(Point2I p1, Point2I p2)
        {
            // Arrange
            var minX = Math.Min(p1.X, p2.X);
            var minY = Math.Min(p1.Y, p2.Y);
            var maxX = Math.Max(p1.X, p2.X);
            var maxY = Math.Max(p1.Y, p2.Y);

            // Act
            var sut = new Rectangle(p1, p2);

            // Assert
            Assert.Equal(minX, sut.Min.X);
            Assert.Equal(minY, sut.Min.Y);
            Assert.Equal(maxX, sut.Max.X);
            Assert.Equal(maxY, sut.Max.Y);
        }

        [Theory]
        [AutoData]
        public void EqualsWithSameTest(Rectangle r1)
        {
            Assert.Equal(r1, r1);
        }

        [Theory]
        [AutoData]
        public void EqualsWithDifferentTest(Rectangle r1, Rectangle r2)
        {
            Assert.NotEqual(r1, r2);
        }

        [Theory]
        [AutoData]
        public void EqualsWithObject(Rectangle r1)
        {
            Assert.True(r1.Equals(r1 as object));
        }

        [Theory]
        [AutoData]
        public void GetHashCodeWorks(Rectangle r1, Rectangle r2)
        {
            Assert.Equal(r1.GetHashCode(), r1.GetHashCode());
            Assert.NotEqual(r1.GetHashCode(), r2.GetHashCode());
        }

        public class OperatorTests
        {
            [Theory]
            [AutoData]
            public void DifferentOperatorTest(Rectangle r1, Rectangle r2)
            {
                Assert.True(r1 != r2);
            }

            [Theory]
            [AutoData]
            public void EqualOperatorTest(Point2I x, Point2I y)
            {
                // ReSharper disable once EqualExpressionComparison
                Assert.True(new Rectangle(x, y) == new Rectangle(x, y));
            }
        }
    }
}