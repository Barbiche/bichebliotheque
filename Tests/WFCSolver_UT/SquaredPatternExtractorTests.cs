using System;
using System.Collections.Generic;
using System.Drawing;
using AutoFixture.Xunit2;
using WFCSolver._Impl;
using WFCSolver.Domain;
using Xunit;

namespace WFCSolver_UT
{
    public class SquaredPatternExtractorTests
    {
        [Theory]
        [AutoData]
        public void ExtractPatternsIsNullGuarded(Sample sample, SizeParameter sizeParameter,
            SquaredPatternExtractor                     sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExtractPatterns(null, sizeParameter));
            Assert.Throws<ArgumentNullException>(() => sut.ExtractPatterns(sample, null));
        }

        [Theory]
        [AutoData]
        public void ExtractPatternsThrowsIfSizeParameterIsGtThanSample(SquaredPatternExtractor sut)
        {
            var sample        = new Sample(new Texture(new Color[4, 4]));
            var sizeParameter = new SizeParameter(5);

            Assert.Throws<ArgumentOutOfRangeException>(() => sut.ExtractPatterns(sample, sizeParameter));
        }

        public static IEnumerable<object[]> PatternsValues()
        {
            yield return new object[]
            {
                new Sample(new Texture(new[,]
                {
                    {Color.Red, Color.Red, Color.Red, Color.Red},
                    {Color.Red, Color.Red, Color.Red, Color.Red},
                    {Color.Red, Color.Red, Color.Red, Color.Red},
                    {Color.Red, Color.Red, Color.Red, Color.Red}
                })),
                2,
                new[]
                {
                    new SquaredPattern(new SquaredTexture(new[,]
                    {
                        {Color.Red, Color.Red},
                        {Color.Red, Color.Red}
                    }))
                }
            };

            yield return new object[]
            {
                new Sample(new Texture(new[,]
                {
                    {Color.Red, Color.White, Color.Yellow, Color.White},
                    {Color.White, Color.Red, Color.White, Color.Yellow},
                    {Color.Red, Color.Blue, Color.Blue, Color.Blue},
                    {Color.Blue, Color.Red, Color.Green, Color.Green}
                })),
                2,
                new[]
                {
                    new SquaredPattern(new SquaredTexture(new[,]
                    {
                        {Color.Red, Color.White},
                        {Color.White, Color.Red}
                    })),
                    new SquaredPattern(new SquaredTexture(new[,]
                    {
                        {Color.Yellow, Color.White},
                        {Color.White, Color.Yellow}
                    })),
                    new SquaredPattern(new SquaredTexture(new[,]
                    {
                        {Color.Red, Color.Blue},
                        {Color.Blue, Color.Red}
                    })),
                    new SquaredPattern(new SquaredTexture(new[,]
                    {
                        {Color.Blue, Color.Blue},
                        {Color.Green, Color.Green}
                    }))
                }
            };
        }

        [Theory]
        [MemberData(nameof(PatternsValues))]
        public void ExtractPatternsWorks(Sample sample, SizeParameter sp, IEnumerable<SquaredPattern> patterns)
        {
            var sut = new SquaredPatternExtractor();

            Assert.Equal(patterns, sut.ExtractPatterns(sample, sp));
        }
    }
}