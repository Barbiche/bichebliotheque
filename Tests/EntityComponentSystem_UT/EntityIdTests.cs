using AutoFixture.Xunit2;
using EntityComponentSystem.ValueObjects;
using Xunit;

namespace EntityComponentSystem_UT
{
    public class EntityIdTests
    {
        [Theory]
        [AutoData]
        public void CreateTest(int value)
        {
            var sut = new EntityId(value);

            Assert.Equal(value, sut);
        }

        [Theory]
        [AutoData]
        public void EqualsWithSameTest(EntityId id)
        {
            Assert.Equal(id, id);
        }

        [Theory]
        [AutoData]
        public void EqualsWithDifferentTest(EntityId id1, EntityId id2)
        {
            Assert.NotEqual(id1, id2);
        }

        [Theory]
        [AutoData]
        public void EqualsWithObject(EntityId id1)
        {
            Assert.True(id1.Equals(id1 as object));
        }

        [Theory]
        [AutoData]
        public void GetHashCodeWorks(EntityId id1, EntityId id2)
        {
            Assert.Equal(id1.GetHashCode(), id1.GetHashCode());
            Assert.NotEqual(id1.GetHashCode(), id2.GetHashCode());
        }

        [Theory]
        [AutoData]
        public void ToStringTest(EntityId id)
        {
            Assert.Equal($"Id: {(int) id}", id.ToString());
        }

        public class OperatorTests
        {
            [Theory]
            [AutoData]
            public void DifferentOperatorTest(EntityId id1, EntityId id2)
            {
                Assert.True(id1 != id2);
            }

            [Theory]
            [AutoData]
            public void EqualOperatorTest(int value)
            {
                // ReSharper disable once EqualExpressionComparison
                Assert.True(new EntityId(value) == new EntityId(value));
            }
        }
    }
}