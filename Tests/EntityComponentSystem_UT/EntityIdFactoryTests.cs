﻿using AutoFixture.Xunit2;
using Xunit;
using EntityComponentSystem._impl;

namespace EntityComponentSystem_UT
{
    public class EntityIdFactoryTests
    {
        [Theory]
        [AutoData]
        public void CreateTest(EntityIdFactory sut)
        {
            Assert.NotEqual(sut.Create(), sut.Create());
        }
    }
}