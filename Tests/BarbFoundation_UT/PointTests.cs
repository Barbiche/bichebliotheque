using AutoFixture.Xunit2;
using BarbFoundation;
using Xunit;

namespace Maths_UT
{
    public class PointTests
    {
        [Theory]
        [AutoData]
        public void CreateTest(int x, int y)
        {
            var sut = new Point2I(x, y);

            Assert.Equal(x, sut.X);
            Assert.Equal(y, sut.Y);
        }

        [Theory]
        [AutoData]
        public void EqualsWithSameTest(Point2I p1)
        {
            Assert.Equal(p1, p1);
        }

        [Theory]
        [AutoData]
        public void EqualsWithDifferentTest(Point2I p1, Point2I p2)
        {
            Assert.NotEqual(p1, p2);
        }

        [Theory]
        [AutoData]
        public void EqualsWithObject(Point2I p1)
        {
            Assert.True(p1.Equals(p1 as object));
        }

        [Theory]
        [AutoData]
        public void GetHashCodeWorks(Point2I p1, Point2I p2)
        {
            Assert.Equal(p1.GetHashCode(), p1.GetHashCode());
            Assert.NotEqual(p1.GetHashCode(), p2.GetHashCode());
        }

        [Theory]
        [AutoData]
        public void ToStringTest(Point2I p)
        {
            Assert.Equal($"X: {p.X}, Y: {p.Y}", p.ToString());
        }

        public class OperatorTests
        {
            [Theory]
            [AutoData]
            public void AdditionTest(Point2I p1, Point2I p2)
            {
                var sut = p1 + p2;

                Assert.Equal(p1.X + p2.X, sut.X);
                Assert.Equal(p1.Y + p2.Y, sut.Y);
            }

            [Theory]
            [AutoData]
            public void SubtractionTest(Point2I p1, Point2I p2)
            {
                var sut = p1 - p2;

                Assert.Equal(p1.X - p2.X, sut.X);
                Assert.Equal(p1.Y - p2.Y, sut.Y);
            }

            [Theory]
            [AutoData]
            public void MultiplicationTest(Point2I p1, Point2I p2)
            {
                var sut = p1 * p2;

                Assert.Equal(p1.X * p2.X, sut.X);
                Assert.Equal(p1.Y * p2.Y, sut.Y);
            }

            [Theory]
            [AutoData]
            public void DivisionTest(Point2I p1, Point2I p2)
            {
                var sut = p1 / p2;

                Assert.Equal(p1.X / p2.X, sut.X);
                Assert.Equal(p1.Y / p2.Y, sut.Y);
            }

            [Theory]
            [AutoData]
            public void DifferentOperatorTest(Point2I p1, Point2I p2)
            {
                Assert.True(p1 != p2);
            }

            [Theory]
            [AutoData]
            public void EqualOperatorTest(int x, int y)
            {
                // ReSharper disable once EqualExpressionComparison
                Assert.True(new Point2I(x, y) == new Point2I(x, y));
            }
        }
    }
}