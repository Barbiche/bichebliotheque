﻿using System;
using BarbFoundation;

namespace _2DShapes
{
    public static class RectangleCalculationExtensions
    {
        public static Rectangle Union(this Rectangle r, Point2I p)
        {
            return new Rectangle(new Point2I(Math.Min(r.Min.X, p.X), Math.Min(r.Min.Y, p.Y)),
                new Point2I(Math.Max(r.Max.X, p.X), Math.Max(r.Max.Y, p.Y)));
        }

        public static Rectangle Union(this Rectangle r1, Rectangle r2)
        {
            return new Rectangle(
                new Point2I(Math.Min(r1.Min.X, r2.Min.X), Math.Min(r1.Min.Y, r2.Min.Y)),
                new Point2I(Math.Max(r1.Max.X, r2.Max.X), Math.Max(r1.Max.Y, r2.Max.Y)));
        }

        public static Rectangle Intersect(this Rectangle r1, Rectangle r2)
        {
            return new Rectangle(
                new Point2I(Math.Max(r1.Min.X, r2.Min.X), Math.Max(r1.Min.Y, r2.Min.Y)),
                new Point2I(Math.Min(r1.Max.X, r2.Max.X), Math.Min(r1.Max.Y, r2.Max.Y)));
        }

        public static bool Overlaps(this Rectangle r1, Rectangle r2)
        {
            var x = r1.Max.X > r2.Min.X && r1.Min.X < r2.Max.X;
            var y = r1.Max.Y > r2.Min.Y && r1.Min.Y < r2.Max.Y;
            return x && y;
        }

        public static bool IsInside(this Point2I p, Rectangle r)
        {
            var x = p.X > r.Min.X && p.X < r.Max.X;
            var y = p.Y > r.Min.Y && p.Y < r.Max.Y;
            return x && y;
        }
    }
}