﻿using System;
using BarbFoundation;
using Equ;

namespace _2DShapes
{
    public readonly struct Rectangle : IEquatable<Rectangle>
    {
        public Point2I Min { get; }
        public Point2I Max { get; }

        public Rectangle(Point2I p1, Point2I p2)
        {
            Min = new Point2I(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y));
            Max = new Point2I(Math.Max(p1.X, p2.X), Math.Max(p1.Y, p2.Y));
        }

        private static readonly MemberwiseEqualityComparer<Rectangle> Comparer =
            MemberwiseEqualityComparer<Rectangle>.ByProperties;

        public static bool operator ==(Rectangle a, Rectangle b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Rectangle a, Rectangle b)
        {
            return !a.Equals(b);
        }

        public bool Equals(Rectangle other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Rectangle other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }
    }
}