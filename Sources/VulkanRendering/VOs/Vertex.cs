﻿using System.Numerics;
using System.Runtime.InteropServices;
using SharpVk;
using SharpVk.Shanq;

namespace VulkanRendering.VOs
{
    public readonly struct Vertex
    {
        public Vertex(Vector2 position, Vector3 color)
        {
            Position = position;
            Color    = color;
        }

        [Location(0)] public readonly Vector2 Position;

        [Location(1)] public readonly Vector3 Color;

        public static VertexInputBindingDescription[] GetBindingDescription()
        {
            return new[]
            {
                new VertexInputBindingDescription(0, (uint) Marshal.SizeOf<Vertex>(), VertexInputRate.Vertex)
            };
        }

        public static VertexInputAttributeDescription[] GetAttributeDescriptions()
        {
            return new[]
            {
                new VertexInputAttributeDescription(0, 0, Format.R32G32SFloat,
                                                    (uint) Marshal.OffsetOf<Vertex>(nameof(Position))),
                new VertexInputAttributeDescription(1, 0, Format.R32G32B32SFloat,
                                                    (uint) Marshal.OffsetOf<Vertex>(nameof(Color)))
            };
        }
    }
}