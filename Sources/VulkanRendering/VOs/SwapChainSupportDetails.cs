﻿using EnsureThat;
using SharpVk.Khronos;

namespace VulkanRendering.VOs
{
    public readonly struct SwapChainSupportDetails
    {
        public readonly SurfaceCapabilities Capabilities;
        public readonly SurfaceFormat[]     Formats;
        public readonly PresentMode[]       PresentModes;

        public SwapChainSupportDetails(SurfaceCapabilities capabilities, SurfaceFormat[] formats,
            PresentMode[]                                  presentModes)
        {
            Ensure.That(formats).IsNotNull();
            Ensure.That(presentModes).IsNotNull();

            Capabilities = capabilities;
            Formats      = formats;
            PresentModes = presentModes;
        }
    }
}