﻿using System.Collections.Generic;
using Functional;

namespace VulkanRendering.VOs
{
    public struct QueueFamilyIndices
    {
        public Option<uint> GraphicsFamily { get; set; }
        public Option<uint> PresentFamily  { get; set; }

        public bool IsComplete()
        {
            return GraphicsFamily.Match(u => true, () => false) && PresentFamily.Match(u => true, () => false);
        }

        public IEnumerable<uint> Indices
        {
            get
            {
                var indices = new List<uint>();
                GraphicsFamily.Match(u => indices.Add(u), () => { });
                PresentFamily.Match(u => indices.Add(u), () => { });
                return indices;
            }
        }
    }
}