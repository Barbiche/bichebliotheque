﻿namespace VulkanSample
{
    internal interface ILoop
    {
        void Run();
    }
}