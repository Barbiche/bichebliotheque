﻿namespace VulkanSample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var vulkanLoop = new VulkanLoop();
            vulkanLoop.Run();
        }
    }
}