﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using EnsureThat;
using Functional;
using SharpVk;
using SharpVk.Glfw;
using SharpVk.Khronos;
using SharpVk.Multivendor;
using VulkanRendering.VOs;
using Buffer = SharpVk.Buffer;
using Version = SharpVk.Version;

namespace VulkanSample
{
    internal class VulkanLoop : ILoop
    {
        private const int Width  = 800;
        private const int Height = 600;

        private readonly Index[] _indices =
        {
            0, 1, 2, 2, 3, 0
        };

        private readonly Vertex[] _vertices =
        {
            new(new Vector2(-0.5f, -0.5f), new Vector3(1.0f, 0.0f, 0.0f)),
            new(new Vector2(0.5f, -0.5f), new Vector3(0.0f, 1.0f, 0.0f)),
            new(new Vector2(0.5f, 0.5f), new Vector3(0.0f, 0.0f, 1.0f)),
            new(new Vector2(-0.5f, 0.5f), new Vector3(1.0f, 1.0f, 1.0f))
        };

        private CommandBuffer[] _commandBuffers;
        private CommandPool     _commandPool;
        private Device          _device;
        private ShaderModule    _fragShader;
        private Framebuffer[]   _framebuffers;
        private Pipeline        _graphicsPipeline;
        private Queue           _graphicsQueue;
        private Semaphore       _imageAvailableSemaphore;
        private ImageView[]     _imageViews;
        private Buffer          _indexBuffer;
        private DeviceMemory    _indexBufferMemory;
        private Instance        _instance;
        private PhysicalDevice  _physicalDevice;
        private PipelineLayout  _pipelineLayout;
        private Queue           _presentQueue;
        private Semaphore       _renderFinishedSemaphore;
        private RenderPass      _renderPass;
        private Surface         _surface;
        private Swapchain       _swapChain;
        private Extent2D        _swapChainExtent;
        private Format          _swapChainFormat;
        private Image[]         _swapChainImages;
        private Buffer          _vertexBuffer;
        private DeviceMemory    _vertexBufferMemory;
        private ShaderModule    _vertexShader;

        private WindowHandle _windowHandle;

        public void Run()
        {
            InitWindow();
            InitVulkan();
            MainLoop();
            Cleanup();
        }

        private QueueFamilyIndices FindQueueFamilies(PhysicalDevice physicalDevice)
        {
            Ensure.That(physicalDevice).IsNotNull();

            var properties         = physicalDevice.GetQueueFamilyProperties();
            var queueFamilyIndices = new QueueFamilyIndices();
            for (uint i = 0; i < properties.Length && !queueFamilyIndices.IsComplete(); i++)
            {
                if (properties[i].QueueFlags.HasFlag(QueueFlags.Graphics))
                {
                    queueFamilyIndices.GraphicsFamily = OptionExtensions.Some(i);
                }

                if (physicalDevice.GetSurfaceSupport(i, _surface))
                {
                    queueFamilyIndices.PresentFamily = OptionExtensions.Some(i);
                }
            }

            return queueFamilyIndices;
        }

        private void InitVulkan()
        {
            CreateVulkanInstance();
            CreateSurface();
            PickPhysicalDevice();
            CreateLogicalDevice();
            CreateSwapChain();
            CreateImageViews();
            CreateShaderModule();
            CreateRenderPass();
            CreateGraphicsPipeline();
            CreateFramebuffers();
            CreateCommandPool();
            CreateVertexBuffer();
            CreateIndexBuffer();
            CreateCommandBuffer();
            CreateSemaphores();
        }

        private void CreateIndexBuffer()
        {
            var indexSize  = Unsafe.SizeOf<Index>();
            var bufferSize = (uint) (indexSize * _indices.Length);
            CreateBuffer(bufferSize, BufferUsageFlags.TransferSource,
                         MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent, out var stagingBuffer,
                         out var stagingBufferMemory);

            var memoryBuffer = stagingBufferMemory.Map(0, bufferSize, MemoryMapFlags.None);

            for (var index = 0; index < _indices.Length; index++)
            {
                Marshal.StructureToPtr(_indices[index], memoryBuffer + indexSize * index, false);
            }

            stagingBufferMemory.Unmap();

            CreateBuffer(bufferSize,
                         BufferUsageFlags.TransferDestination | BufferUsageFlags.IndexBuffer,
                         MemoryPropertyFlags.DeviceLocal,
                         out _indexBuffer,
                         out _indexBufferMemory);

            CopyBuffer(stagingBuffer, _indexBuffer, bufferSize);

            stagingBuffer.Dispose();
            stagingBufferMemory.Free();
        }

        private void CreateVertexBuffer()
        {
            var vertexSize = Unsafe.SizeOf<Vertex>();
            var bufferSize = (uint) (vertexSize * _vertices.Length);
            CreateBuffer(bufferSize, BufferUsageFlags.TransferSource,
                         MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent, out var stagingBuffer,
                         out var stagingBufferMemory);

            var memoryBuffer = stagingBufferMemory.Map(0, bufferSize, MemoryMapFlags.None);

            for (var index = 0; index < _vertices.Length; index++)
            {
                Marshal.StructureToPtr(_vertices[index], memoryBuffer + vertexSize * index, false);
            }

            stagingBufferMemory.Unmap();

            CreateBuffer(bufferSize,
                         BufferUsageFlags.TransferDestination | BufferUsageFlags.VertexBuffer,
                         MemoryPropertyFlags.DeviceLocal,
                         out _vertexBuffer,
                         out _vertexBufferMemory);

            CopyBuffer(stagingBuffer, _vertexBuffer, bufferSize);

            stagingBuffer.Dispose();
            stagingBufferMemory.Free();
        }

        private void CopyBuffer(Buffer sourceBuffer, Buffer destinationBuffer, ulong size)
        {
            var transferBuffers = _device.AllocateCommandBuffers(_commandPool, CommandBufferLevel.Primary, 1);

            transferBuffers[0].Begin(CommandBufferUsageFlags.OneTimeSubmit);

            transferBuffers[0].CopyBuffer(sourceBuffer, destinationBuffer, new BufferCopy {Size = size});

            transferBuffers[0].End();

            _graphicsQueue.Submit(new SubmitInfo
            {
                CommandBuffers = transferBuffers
            }, null);
            _graphicsQueue.WaitIdle();

            _commandPool.FreeCommandBuffers(transferBuffers);
        }

        private void CreateBuffer(ulong               size,
                                  BufferUsageFlags    usage,
                                  MemoryPropertyFlags properties,
                                  out Buffer          buffer,
                                  out DeviceMemory    bufferMemory)
        {
            buffer = _device.CreateBuffer(size, usage, SharingMode.Exclusive, null);
            var memReq = buffer.GetMemoryRequirements();

            bufferMemory = _device.AllocateMemory(memReq.Size, FindMemoryType(memReq.MemoryTypeBits, properties));

            buffer.BindMemory(bufferMemory, 0);
        }

        private uint FindMemoryType(uint typeFilter, MemoryPropertyFlags flags)
        {
            var memoryProperties = _physicalDevice.GetMemoryProperties();
            for (var i = 0; i < memoryProperties.MemoryTypes.Length; i++)
            {
                if ((typeFilter & (1u << i)) > 0 && memoryProperties.MemoryTypes[i].PropertyFlags.HasFlag(flags))
                {
                    return (uint) i;
                }
            }

            throw new Exception("No compatible memory type.");
        }

        private void CreateSemaphores()
        {
            _imageAvailableSemaphore = _device.CreateSemaphore();
            _renderFinishedSemaphore = _device.CreateSemaphore();
        }

        private void CreateCommandBuffer()
        {
            _commandBuffers =
                _device.AllocateCommandBuffers(_commandPool, CommandBufferLevel.Primary, (uint) _framebuffers.Length);

            for (var i = 0; i < _commandBuffers.Length; i++)
            {
                var commandBuffer = _commandBuffers[i];
                commandBuffer.Begin();
                commandBuffer.BeginRenderPass(_renderPass, _framebuffers[i], new Rect2D(_swapChainExtent),
                                              new ClearValue(), SubpassContents.Inline);
                commandBuffer.BindPipeline(PipelineBindPoint.Graphics, _graphicsPipeline);

                commandBuffer.BindVertexBuffers(0, _vertexBuffer, (DeviceSize) 0);
                commandBuffer.BindIndexBuffer(_indexBuffer, 0, IndexType.Uint32);

                commandBuffer.DrawIndexed((uint) _indices.Length, 1, 0, 0, 0);

                commandBuffer.EndRenderPass();
                commandBuffer.End();
            }
        }

        private void CreateCommandPool()
        {
            var queueFamilyIndices = FindQueueFamilies(_physicalDevice);

            var indexValue = queueFamilyIndices.GraphicsFamily.Proj(0);

            _commandPool = _device.CreateCommandPool(indexValue);
        }

        private void CreateFramebuffers()
        {
            _framebuffers = new Framebuffer[_imageViews.Length];

            for (var i = 0; i < _imageViews.Length; i++)
            {
                _framebuffers[i] = _device.CreateFramebuffer(_renderPass, new[] {_imageViews[i]},
                                                             _swapChainExtent.Width,
                                                             _swapChainExtent.Height, 1);
            }
        }

        private void CreateRenderPass()
        {
            var colorAttachment = new AttachmentDescription
            {
                Format         = _swapChainFormat,
                Samples        = SampleCountFlags.SampleCount1,
                LoadOp         = AttachmentLoadOp.Clear,
                StoreOp        = AttachmentStoreOp.Store,
                StencilLoadOp  = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout  = ImageLayout.Undefined,
                FinalLayout    = ImageLayout.PresentSource
            };

            var colorAttachmentRef = new AttachmentReference
            {
                Attachment = 0,
                Layout     = ImageLayout.ColorAttachmentOptimal
            };

            var subpass = new SubpassDescription
            {
                PipelineBindPoint = PipelineBindPoint.Graphics,
                ColorAttachments  = new[] {colorAttachmentRef},
            };

            var subpassDependency = new SubpassDependency
            {
                SourceSubpass         = Constants.SubpassExternal,
                DestinationSubpass    = 0,
                SourceStageMask       = PipelineStageFlags.ColorAttachmentOutput,
                SourceAccessMask      = 0,
                DestinationStageMask  = PipelineStageFlags.ColorAttachmentOutput,
                DestinationAccessMask = AccessFlags.ColorAttachmentWrite
            };

            _renderPass = _device.CreateRenderPass(new[] {colorAttachment}, new[] {subpass},
                                                   new[] {subpassDependency});
        }

        private void CreateShaderModule()
        {
            ShaderModule CreateShader(string path)
            {
                var shaderData = LoadShaderData(path, out var codeSize);
                return _device.CreateShaderModule(codeSize, shaderData);
            }

            _vertexShader = CreateShader(@".\Shaders\vert.spv");
            _fragShader   = CreateShader(@".\Shaders\frag.spv");
        }

        private static uint[] LoadShaderData(string filePath, out int codeSize)
        {
            var fileBytes  = File.ReadAllBytes(filePath);
            var shaderData = new uint[(int) Math.Ceiling(fileBytes.Length / 4f)];

            System.Buffer.BlockCopy(fileBytes, 0, shaderData, 0, fileBytes.Length);

            codeSize = fileBytes.Length;

            return shaderData;
        }

        private void CreateGraphicsPipeline()
        {
            var shaderStages = new[]
            {
                new PipelineShaderStageCreateInfo
                {
                    Stage  = ShaderStageFlags.Vertex,
                    Module = _vertexShader,
                    Name   = "main",
                },
                new PipelineShaderStageCreateInfo
                {
                    Stage  = ShaderStageFlags.Fragment,
                    Module = _fragShader,
                    Name   = "main"
                }
            };

            var pipelineVertexInputStateCreateInfo = new PipelineVertexInputStateCreateInfo
            {
                VertexBindingDescriptions   = Vertex.GetBindingDescription(),
                VertexAttributeDescriptions = Vertex.GetAttributeDescriptions()
            };

            var inputAssemblyStateCreateInfo = new PipelineInputAssemblyStateCreateInfo
            {
                PrimitiveRestartEnable = false,
                Topology               = PrimitiveTopology.TriangleList
            };

            var viewPort = new Viewport(0f, 0f, _swapChainExtent.Width, _swapChainExtent.Height, 0f, 1f);
            var scissor  = new Rect2D(_swapChainExtent);

            var viewportState = new PipelineViewportStateCreateInfo
            {
                Scissors = new[] {scissor}, Viewports = new[] {viewPort}
            };

            var rasterizerState = new PipelineRasterizationStateCreateInfo
            {
                DepthClampEnable = false, RasterizerDiscardEnable = false, PolygonMode = PolygonMode.Fill,
                LineWidth        = 1.0f, CullMode                 = CullModeFlags.Back, FrontFace = FrontFace.Clockwise,
                DepthBiasEnable  = false, DepthBiasConstantFactor = 0f, DepthBiasClamp = 0f, DepthBiasSlopeFactor = 0f
            };

            var multisampling = new PipelineMultisampleStateCreateInfo
            {
                SampleShadingEnable   = false,
                RasterizationSamples  = SampleCountFlags.SampleCount1,
                MinSampleShading      = 1f,
                SampleMask            = null,
                AlphaToCoverageEnable = false,
                AlphaToOneEnable      = false
            };

            var colorBlendAttachment = new PipelineColorBlendAttachmentState
            {
                ColorWriteMask = ColorComponentFlags.R | ColorComponentFlags.G | ColorComponentFlags.B |
                                 ColorComponentFlags.A,
                BlendEnable                 = false,
                SourceColorBlendFactor      = BlendFactor.One,
                DestinationColorBlendFactor = BlendFactor.Zero,
                ColorBlendOp                = BlendOp.Add,
                SourceAlphaBlendFactor      = BlendFactor.One,
                DestinationAlphaBlendFactor = BlendFactor.Zero,
                AlphaBlendOp                = BlendOp.Add
            };

            var colorBlending = new PipelineColorBlendStateCreateInfo
            {
                LogicOpEnable  = false,
                LogicOp        = LogicOp.Copy,
                Attachments    = new[] {colorBlendAttachment},
                BlendConstants = (0, 0, 0, 0)
            };

            _pipelineLayout = _device.CreatePipelineLayout(null, null);

            _graphicsPipeline = _device.CreateGraphicsPipeline(null, shaderStages,
                                                               pipelineVertexInputStateCreateInfo,
                                                               inputAssemblyStateCreateInfo, rasterizerState,
                                                               _pipelineLayout,
                                                               _renderPass, 0, null, -1, viewportState: viewportState,
                                                               colorBlendState: colorBlending,
                                                               multisampleState: multisampling);
        }

        private void CreateImageViews()
        {
            _imageViews = new ImageView[_swapChainImages.Length];
            for (var i = 0; i < _swapChainImages.Length; i++)
            {
                _imageViews[i] = _device.CreateImageView(_swapChainImages[i], ImageViewType.ImageView2d,
                                                         _swapChainFormat,
                                                         ComponentMapping.Identity,
                                                         new ImageSubresourceRange(ImageAspectFlags.Color, 0, 1, 0, 1));
            }
        }

        private void CreateSwapChain()
        {
            var swapChainSupport = QuerySwapChainSupport(_physicalDevice);

            var surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.Formats);
            var presentMode   = ChooseSwapPresentMode(swapChainSupport.PresentModes);
            var extent        = ChooseSwapExtent(swapChainSupport.Capabilities);

            var imageCount = swapChainSupport.Capabilities.MinImageCount + 1;
            if (swapChainSupport.Capabilities.MaxImageCount > 0 &&
                imageCount                                  > swapChainSupport.Capabilities.MaxImageCount)
            {
                imageCount = swapChainSupport.Capabilities.MaxImageCount;
            }

            var indices            = FindQueueFamilies(_physicalDevice);
            var imageSharingMode   = SharingMode.Exclusive;
            var queueFamilyIndices = new List<uint>().ToArray();
            indices.PresentFamily.Match(presentIndex =>
            {
                indices.GraphicsFamily.Match(graphicsIndex =>
                {
                    if (presentIndex != graphicsIndex)
                    {
                        imageSharingMode   = SharingMode.Concurrent;
                        queueFamilyIndices = new[] {graphicsIndex, presentIndex};
                    }
                }, () => { });
            }, () => { });

            _swapChain = _device.CreateSwapchain(_surface, imageCount, surfaceFormat.Format, surfaceFormat.ColorSpace,
                                                 extent, 1, ImageUsageFlags.ColorAttachment, imageSharingMode,
                                                 queueFamilyIndices,
                                                 swapChainSupport.Capabilities.CurrentTransform,
                                                 CompositeAlphaFlags.Opaque, presentMode, true, null);

            _swapChainImages = _swapChain.GetImages();
            _swapChainFormat = surfaceFormat.Format;
            _swapChainExtent = extent;
        }

        private void CreateSurface()
        {
            _surface = _instance.CreateGlfw3Surface(_windowHandle);
        }

        private void CreateLogicalDevice()
        {
            var indices = FindQueueFamilies(_physicalDevice);

            _device = _physicalDevice.CreateDevice(indices.Indices.Select(index => new DeviceQueueCreateInfo
            {
                QueueFamilyIndex = index,
                QueuePriorities  = new[] {1f}
            }).ToArray(), null, KhrExtensions.Swapchain);

            indices.GraphicsFamily.Match(u => _graphicsQueue = _device.GetQueue(u, 0), () => { });
            indices.PresentFamily.Match(u => _presentQueue   = _device.GetQueue(u, 0), () => { });
        }

        private void PickPhysicalDevice()
        {
            var availableDevices = _instance.EnumeratePhysicalDevices();
            _physicalDevice = availableDevices.First(IsSuitableDevice);
        }

        private bool IsSuitableDevice(PhysicalDevice physicalDevice)
        {
            return physicalDevice.EnumerateDeviceExtensionProperties(null)
                       .Any(extension => extension.ExtensionName == KhrExtensions.Swapchain) &&
                   FindQueueFamilies(physicalDevice).IsComplete();
        }

        private void CreateVulkanInstance()
        {
            var enabledLayers = new List<string>();

            void AddAvailableLayer(string layerName)
            {
                if (Instance.EnumerateLayerProperties().Any(x => x.LayerName == layerName))
                {
                    enabledLayers.Add(layerName);
                }
            }

            AddAvailableLayer("VK_LAYER_LUNARG_standard_validation");

            try
            {
                _instance = Instance.Create(enabledLayers.ToArray(),
                                            Glfw3.GetRequiredInstanceExtensions().Append(ExtExtensions.DebugReport)
                                                .ToArray(),
                                            applicationInfo: new ApplicationInfo
                                            {
                                                ApiVersion         = new Version(1, 0, 0),
                                                ApplicationName    = "Vulkan Sample",
                                                ApplicationVersion = new Version(1, 0, 0),
                                                EngineName         = "BarbEngine",
                                                EngineVersion      = new Version(1, 0, 0)
                                            });

                _instance.CreateDebugReportCallback(DebugReportDelegate,
                                                    DebugReportFlags.Error | DebugReportFlags.Warning);
            }
            catch (SharpVkException vkException)
            {
                Console.WriteLine(vkException);
            }
        }

        private SurfaceFormat ChooseSwapSurfaceFormat(SurfaceFormat[] availableFormats)
        {
            foreach (var availableFormat in availableFormats)
            {
                if (availableFormat.Format     == Format.B8G8R8A8Srgb &&
                    availableFormat.ColorSpace == ColorSpace.SrgbNonlinear)
                {
                    return availableFormat;
                }
            }

            return availableFormats.First();
        }

        private PresentMode ChooseSwapPresentMode(PresentMode[] availableModes)
        {
            foreach (var availableMode in availableModes)
            {
                if (availableMode == PresentMode.Mailbox)
                {
                    return availableMode;
                }
            }

            return PresentMode.Fifo;
        }

        private Extent2D ChooseSwapExtent(SurfaceCapabilities capabilities)
        {
            if (capabilities.CurrentExtent.Width != uint.MaxValue)
            {
                return capabilities.CurrentExtent;
            }

            var actualExtent = new Extent2D(Width, Height);

            actualExtent.Width = Math.Max(capabilities.MinImageExtent.Width,
                                          Math.Min(capabilities.MaxImageExtent.Width, actualExtent.Width));
            actualExtent.Height = Math.Max(capabilities.MinImageExtent.Height,
                                           Math.Min(capabilities.MaxImageExtent.Height, actualExtent.Height));

            return actualExtent;
        }

        private SwapChainSupportDetails QuerySwapChainSupport(PhysicalDevice device)
        {
            return new SwapChainSupportDetails(device.GetSurfaceCapabilities(_surface),
                                               device.GetSurfaceFormats(_surface),
                                               device.GetSurfacePresentModes(_surface));
        }

        private Bool32 DebugReportDelegate(DebugReportFlags flags, DebugReportObjectType objectType, ulong o,
                                           HostSize location, int messageCode, string playerPrefix, string message,
                                           IntPtr userData)
        {
            Console.WriteLine(message);

            return false;
        }


        private void Cleanup()
        {
            _imageAvailableSemaphore.Dispose();
            _imageAvailableSemaphore = null;

            _renderFinishedSemaphore.Dispose();
            _renderFinishedSemaphore = null;

            _commandPool.Dispose();
            _commandPool = null;

            foreach (var framebuffer in _framebuffers)
            {
                framebuffer.Dispose();
            }

            _framebuffers = null;

            _graphicsPipeline.Dispose();
            _graphicsPipeline = null;

            _pipelineLayout.Dispose();
            _pipelineLayout = null;

            _renderPass.Dispose();
            _renderPass = null;

            _fragShader.Dispose();
            _fragShader = null;

            _vertexShader.Dispose();
            _vertexShader = null;

            foreach (var imageView in _imageViews)
            {
                imageView.Dispose();
            }

            _imageViews = null;

            _swapChain.Dispose();
            _swapChain = null;

            _vertexBuffer.Dispose();
            _vertexBuffer = null;

            _indexBuffer.Dispose();
            _indexBuffer = null;

            _surface.Dispose();
            _surface = null;

            _instance.Dispose();
            _instance = null;

            Glfw3.Terminate();
        }

        private void MainLoop()
        {
            while (!Glfw3.WindowShouldClose(_windowHandle))
            {
                Glfw3.PollEvents();
                DrawFrame();
            }
        }

        private void DrawFrame()
        {
            var nextImage = _swapChain.AcquireNextImage(ulong.MaxValue, _imageAvailableSemaphore, null);
            _graphicsQueue.Submit(new SubmitInfo
            {
                WaitSemaphores           = new[] {_imageAvailableSemaphore},
                WaitDestinationStageMask = new[] {PipelineStageFlags.ColorAttachmentOutput},
                CommandBuffers           = new[] {_commandBuffers[nextImage]},
                SignalSemaphores         = new[] {_renderFinishedSemaphore}
            }, null);

            _presentQueue.Present(new[] {_renderFinishedSemaphore}, new[] {_swapChain}, nextImage, new Result[1]);
        }

        private void InitWindow()
        {
            Glfw3.Init();

            Glfw3.WindowHint(0x00022001, 0);
            _windowHandle = Glfw3.CreateWindow(Width, Height, "Appli", IntPtr.Zero, IntPtr.Zero);

            Glfw3.SetWindowSizeCallback(_windowHandle, RecreateSwapChain);
        }

        private void RecreateSwapChain(WindowHandle window, int width, int height) { }
    }
}