﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Dom.Meshes;
using IO.Meshes;
using ObjLoader.Loader.Loaders;

namespace MeshLoader
{
    public class MeshLoader : IMeshLoader
    {
        private readonly Lazy<IObjLoaderFactory> _lazyFactory =
            new Lazy<IObjLoaderFactory>(() => new ObjLoaderFactory());

        private readonly IVerticesRepository _verticesRepository;

        public MeshLoader(IVerticesRepository verticesRepository)
        {
            _verticesRepository = verticesRepository;
        }

        public IEnumerable<TriangleMesh> Load(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }

            var loader = _lazyFactory.Value.Create();

            using var fileStream = new FileStream(path, FileMode.Open);
            var       results    = loader.Load(fileStream);

            return CreateFromLoadResult(results);
        }

        private IEnumerable<TriangleMesh> CreateFromLoadResult(LoadResult results)
        {
            var vbi = _verticesRepository.Set(results.Vertices.Select(v => new Vertex(new Vector3(v.X, v.Y, v.Z)))
                                                  .ToArray());

            foreach (var group in results.Groups)
            {
                var indexBuffer = new List<Index>();
                foreach (var face in group.Faces)
                {
                    if (face.Count == 3)
                    {
                        indexBuffer.Add(new Index(face[0].VertexIndex));
                        indexBuffer.Add(new Index(face[1].VertexIndex));
                        indexBuffer.Add(new Index(face[2].VertexIndex));
                    }
                    else
                    {
                        throw new InvalidOperationException("Obj is not a triangle mesh.");
                    }
                }

                yield return new TriangleMesh(vbi, indexBuffer.ToArray());
            }
        }
    }
}