﻿using System.IO;
using System.Linq;
using BarbFoundation;
using EnsureThat;
using Functional;
using IO.Images;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoResourcesLoader
{
    public sealed class TextureDataLoader : IImageLoader
    {
        private readonly GraphicsDevice _graphicsDevice;

        public TextureDataLoader(GraphicsDevice graphicsDevice)
        {
            Ensure.That(graphicsDevice).IsNotNull();

            _graphicsDevice = graphicsDevice;
        }

        public Option<Image> Load(string path)
        {
            using (var fileStream =
                new FileStream(
                    path,
                    FileMode.Open))
            {
                using (var texture = Texture2D.FromStream(_graphicsDevice, fileStream))
                {
                    var data = new Color[texture.Height * texture.Width];
                    texture.GetData(data);
                    return OptionExtensions.Some(new Image(
                        data.Select(color => System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B))
                            .ToArray(), new Width(texture.Width), new Height(texture.Height)));
                }
            }
        }
    }
}