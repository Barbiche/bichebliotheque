﻿using System.IO;
using BarbFoundation;
using EnsureThat;
using IO.Images;
using Microsoft.Xna.Framework.Graphics;

namespace MonoResourcesLoader
{
    public sealed class TextureDataSaver : IImageSaver
    {
        private readonly GraphicsDevice _graphicsDevice;

        public TextureDataSaver(GraphicsDevice graphicsDevice)
        {
            Ensure.That(graphicsDevice).IsNotNull();

            _graphicsDevice = graphicsDevice;
        }

        public void Save(Image pixels, string imagePath)
        {
            Ensure.That(pixels).IsNotNull();

            using (var renderTarget = new RenderTarget2D(_graphicsDevice, pixels.Width, pixels.Height))
            {
                renderTarget.SetData(pixels.Pixels.ToArray());

                using (var fs = File.Create(imagePath))
                {
                    renderTarget.SaveAsPng(fs, pixels.Width, pixels.Height);
                }
            }
        }
    }
}