﻿using BarbFoundation;
using Functional;

namespace Inf.Images.Bitmap
{
    public interface IBitmapReader
    {
        Option<Image> Read(BitmapImage imagePath);
    }
}