﻿using System.Collections.Generic;
using System.Linq;
using EnsureThat;
using Equ;
using IO;

namespace Inf.Images.Bitmap
{
    public sealed class BitmapImage : MemberwiseEquatable<BitmapImage>
    {
        private static readonly IEnumerable<FileExtension> CompatibleExtensions = new[]
        {
            new FileExtension(".bmp"),
            new FileExtension(".gif"),
            new FileExtension(".exif"),
            new FileExtension(".jpg"),
            new FileExtension(".png"),
            new FileExtension(".tiff"),
        };

        private readonly FilePath _path;

        public BitmapImage(FilePath path)
        {
            EnsureArg.IsNotNull(path, nameof(path));
            EnsureArg.IsTrue(CompatibleExtensions.Contains(path.Extension), nameof(path),
                opts => opts.WithMessage($"{path.Extension} is not compatible for a {nameof(BitmapImage)}"));
            _path = path;
        }

        public override string ToString()
        {
            return _path.ToString();
        }
    }
}