﻿using BarbFoundation;

namespace Inf.Images.Bitmap
{
    public interface IBitmapSaver
    {
        void Save(Image image, BitmapImage imagePath);
    }
}