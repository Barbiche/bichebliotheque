﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using EnsureThat;
using Image = BarbFoundation.Image;

namespace Inf.Images.Bitmap
{
    public class BitmapSaver : IBitmapSaver
    {
        public unsafe void Save(Image image, BitmapImage bitmapImage)
        {
            EnsureArg.IsNotNull(image);
            EnsureArg.IsNotNull(bitmapImage);

            using (var bitmap = new System.Drawing.Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb))
            {
                var bytesToCopy = image.Width * image.Height * 4;

                var pixels = GCHandle.Alloc(image.Pixels.ToArray(), GCHandleType.Pinned);
                var data = bitmap.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.WriteOnly,
                    PixelFormat.Format32bppArgb);
                try
                {
                    var sourcePtr = pixels.AddrOfPinnedObject();
                    Buffer.MemoryCopy((void*) sourcePtr, data.Scan0.ToPointer(), bytesToCopy, bytesToCopy);
                }
                finally
                {
                    pixels.Free();
                }

                bitmap.UnlockBits(data);

                bitmap.Save(bitmapImage.ToString());
            }
        }
    }
}