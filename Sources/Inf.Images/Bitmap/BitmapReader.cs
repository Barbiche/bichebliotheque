﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using BarbFoundation;
using EnsureThat;
using Functional;
using Image = BarbFoundation.Image;

namespace Inf.Images.Bitmap
{
    public sealed class BitmapReader : IBitmapReader
    {
        public unsafe Option<Image> Read(BitmapImage imagePath)
        {
            EnsureArg.IsNotNull(imagePath);

            using (var bitmap = new System.Drawing.Bitmap(imagePath.ToString()))
            {
                var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly,
                    PixelFormat.Format32bppArgb);
                var width      = new Width(bitmap.Width);
                var height     = new Height(bitmap.Height);
                var length     = width * height * 4;
                var imageArray = new Color[width * height];

                fixed (Color* bitsPtr = &imageArray[0])
                {
                    Buffer.MemoryCopy(data.Scan0.ToPointer(), bitsPtr, length, length);
                }

                bitmap.UnlockBits(data);

                return OptionExtensions.Some(new Image(imageArray, width, height));
            }
        }
    }
}