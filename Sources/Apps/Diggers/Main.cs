﻿using EntityComponentSystem;
using EntityComponentSystem._impl;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Diggers
{
    public sealed class Main : Game
    {
        private readonly ISystemRunner         _systemRunner;
        private readonly IWorld                _world;
        private          GraphicsDeviceManager graphics;
        private          SpriteBatch           spriteBatch;
        private          Texture2D             texture;

        public Main()
        {
            graphics              = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Assets\\bin";

            // ECS creation

            var entityFactory = new EntityFactory(new EntityIdFactory());
            var systemStore   = new SystemStore();
            _systemRunner = systemStore;
            _world        = new World(entityFactory, new ComponentStore(entityFactory.CreatedObject), systemStore);
            _world.AddSystem(new TestSystem());
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            texture     = Content.Load<Texture2D>("char_0");
        }

        protected override void UnloadContent()
        {
            //texture.Dispose(); <-- Only directly loaded
            Content.Unload();
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            _systemRunner.Execute();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            spriteBatch.Draw(texture, Vector2.Zero);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}