﻿using System;
using EntityComponentSystem;

namespace Diggers
{
    public class TestSystem : IExecutableSystem
    {
        public void Execute()
        {
            Console.WriteLine("TestSystem is ticking.");
        }
    }
}