﻿namespace MonoWFC
{
    public static class Start
    {
        private static void Main(string[] args)
        {
            using (var game = new MonoWfc())
            {
                game.Run();
            }
        }
    }
}