﻿using BarbFoundation;
using BarbFoundation.DataStructures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoResourcesLoader;
using WFCSolver._Impl;

namespace MonoWFC
{
    public class MonoWfc : Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private          SpriteBatch           _spriteBatch;

        public MonoWfc()
        {
            _graphics = new GraphicsDeviceManager(this)
            {
                IsFullScreen = false
            };

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            var wfcSolver = new WfcSolver(new SampleFactory(new TextureDataLoader(_graphics.GraphicsDevice)),
                new GridFactory(new DomainFactory()), new SquaredPatternExtractor());
            wfcSolver.Solve(
                    "C:\\Users\\gueth\\source\\repos\\Monogame\\Bichebliothèque\\Sources\\Apps\\MonoWFC\\logo_fliped.png",
                    128,
                    128)
                .Match(
                    colors =>
                    {
                        var textureSaver = new TextureDataSaver(_graphics.GraphicsDevice);
                        textureSaver.Save(
                            new Image(colors.ToSingleDimension(), new Width(colors.GetLength(0)),
                                new Height(colors.GetLength(1))), "result.png");
                    }, () => { });
        }


        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }


        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }
    }
}