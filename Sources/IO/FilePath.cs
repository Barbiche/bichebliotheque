﻿using System.IO;
using EnsureThat;
using Equ;

namespace IO
{
    public sealed class FilePath : MemberwiseEquatable<FilePath>
    {
        public FilePath(string path)
        {
            EnsureArg.IsNotNullOrWhiteSpace(path);

            path = path.Replace('/', Path.DirectorySeparatorChar);
            path = path.Replace('\\', Path.DirectorySeparatorChar);

            Extension = new FileExtension(Path.GetExtension(path));
            Value     = path;
        }

        public FileExtension Extension { get; set; }
        public string        Value     { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}