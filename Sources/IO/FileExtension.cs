﻿using System;
using EnsureThat;

namespace IO
{
    public sealed class FileExtension : IEquatable<FileExtension>
    {
        private readonly string _value;

        public FileExtension(string extension)
        {
            EnsureArg.IsNotNullOrWhiteSpace(extension, nameof(extension),
                opts => opts.WithMessage($"{nameof(extension)} must be a non empty string."));
            EnsureArg.StartsWith(extension, ".", nameof(extension),
                opts => opts.WithMessage($"{nameof(extension)} has to start with a '.'."));

            _value = extension;
        }

        public bool Equals(FileExtension other)
        {
            if (other is null)
            {
                return false;
            }

            return ReferenceEquals(this, other) || _value.Equals(other._value, StringComparison.OrdinalIgnoreCase);
        }

        public static implicit operator string(FileExtension type)
        {
            return type._value;
        }

        public static explicit operator FileExtension(string extension)
        {
            return new FileExtension(extension);
        }

        public static bool operator ==(in FileExtension left, in FileExtension right)
        {
            if (left is null)
            {
                return right is null;
            }

            return left.Equals(right);
        }

        public static bool operator !=(FileExtension left, FileExtension right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as FileExtension);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
    }
}