﻿using BarbFoundation;

namespace IO.Images
{
    public interface IImageSaver
    {
        void Save(Image image, string imagePath);
    }
}