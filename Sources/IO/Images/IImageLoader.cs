﻿using BarbFoundation;
using Functional;

namespace IO.Images
{
    public interface IImageLoader
    {
        Option<Image> Load(string path);
    }
}