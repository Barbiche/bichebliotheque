﻿using System.Collections.Generic;
using Dom.Meshes;

namespace IO.Meshes
{
    public interface IMeshLoader
    {
        IEnumerable<TriangleMesh> Load(string path);
    }
}