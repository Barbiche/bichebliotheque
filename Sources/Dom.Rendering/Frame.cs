﻿using EnsureThat;
using Equ;
using Microsoft.Xna.Framework;

namespace Dom.Rendering
{
    public class Frame : MemberwiseEquatable<Frame>
    {
        public Frame(Color[,] pixels)
        {
            Ensure.That(pixels.GetLength(0)).IsGt(0);
            Ensure.That(pixels.GetLength(1)).IsGt(0);

            Width  = Pixels.GetLength(0);
            Height = Pixels.GetLength(1);

            Pixels = pixels;
        }

        public Color[,] Pixels { get; }
        public int      Width  { get; }
        public int      Height { get; }
        public Color this[PixelCoordinate coordinate] => Pixels[coordinate.X, coordinate.Y];
    }
}