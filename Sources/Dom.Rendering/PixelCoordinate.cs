﻿using EnsureThat;
using Equ;

namespace Dom.Rendering
{
    public class PixelCoordinate : MemberwiseEquatable<PixelCoordinate>
    {
        public PixelCoordinate(int x, int y)
        {
            Ensure.That(x).IsGte(0);
            Ensure.That(y).IsGte(0);

            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }
    }
}