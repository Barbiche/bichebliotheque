﻿using System;
using System.Collections.Generic;
using System.Drawing;
using EnsureThat;
using Equ;

namespace BarbFoundation
{
    public sealed class Image : MemberwiseEquatable<Image>
    {
        private readonly Color[] _pixels;

        public Image(Color[] pixels, Width width, Height height)
        {
            EnsureArg.IsNotNull(pixels);
            EnsureArg.HasItems((IReadOnlyCollection<Color>) pixels);

            if (width * height != pixels.Length)
            {
                throw new ArgumentException(
                    $"Width {width} and Height {height} don't match with pixel count {pixels.Length}.");
            }

            _pixels = pixels;
            Width   = width;
            Height  = height;
        }

        public ReadOnlySpan<Color> Pixels => _pixels;
        public Width               Width  { get; }
        public Height              Height { get; }
    }
}