﻿namespace BarbFoundation
{
    public static class OptionExtensions
    {
        public static Option<TValue> None<TValue>()
        {
            return Option<TValue>.None;
        }

        public static Option<TValue> Some<TValue>(TValue value)
        {
            return Option<TValue>.Some(value);
        }
    }
}