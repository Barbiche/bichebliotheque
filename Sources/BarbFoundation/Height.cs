﻿using System;
using EnsureThat;
using Equ;

namespace BarbFoundation
{
    public readonly struct Height : IEquatable<Height>
    {
        private readonly int _value;

        public Height(int value)
        {
            Ensure.That(value).IsGt(0);

            _value = value;
        }

        public static explicit operator Height(int value)
        {
            return new Height(value);
        }

        public static implicit operator int(Height width)
        {
            return width._value;
        }

        private static readonly MemberwiseEqualityComparer<Height> Comparer =
            MemberwiseEqualityComparer<Height>.ByFields;

        public bool Equals(Height other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Height other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }
    }
}