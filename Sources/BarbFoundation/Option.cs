﻿using System;
using System.Collections.Generic;
using Equ;

namespace BarbFoundation
{
    public readonly struct Option<TValue> : IEquatable<Option<TValue>>
    {
        private static readonly IEqualityComparer<Option<TValue>> Comparer =
            MemberwiseEqualityComparer<Option<TValue>>.ByFields;

        private readonly bool   _isNotNone;
        private readonly TValue _value;

        private Option(TValue value)
        {
            _isNotNone = true;
            _value     = value;
        }

        internal static Option<TValue> None { get; } = new Option<TValue>();

        internal static Option<TValue> Some(TValue value)
        {
            return new Option<TValue>(value);
        }

        public void WhenSome(Action<TValue> valueAction)
        {
            if (_isNotNone)
            {
                valueAction(_value);
            }
        }

        public TValue Proj(Func<TValue> noneFunc)
        {
            return _isNotNone ? _value : noneFunc();
        }

        public TValue Proj(TValue noneValue)
        {
            return _isNotNone ? _value : noneValue;
        }

        public Option<TOut> FMap<TOut>(Func<TValue, TOut> f)
        {
            return _isNotNone ? Option<TOut>.Some(f(_value)) : Option<TOut>.None;
        }

        public Option<TOut> Bind<TOut>(Func<TValue, Option<TOut>> f)
        {
            return _isNotNone ? f(_value) : Option<TOut>.None;
        }

        public TOut Match<TOut>(Func<TValue, TOut> someCase, Func<TOut> noneCase)
        {
            return _isNotNone ? someCase(_value) : noneCase();
        }

        public void Match(Action<TValue> someCase, Action noneCase)
        {
            if (_isNotNone)
            {
                someCase(_value);
            }
            else
            {
                noneCase();
            }
        }

        public bool Equals(Option<TValue> other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Option<TValue> other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }

        public static bool operator ==(Option<TValue> left, Option<TValue> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Option<TValue> left, Option<TValue> right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return _isNotNone ? $"Some ({_value}" : $"None<{typeof(TValue)}>()";
            ;
        }
    }
}