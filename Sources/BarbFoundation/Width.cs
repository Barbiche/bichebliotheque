﻿using System;
using EnsureThat;
using Equ;

namespace BarbFoundation
{
    public readonly struct Width : IEquatable<Width>
    {
        private readonly int _value;

        public Width(int value)
        {
            Ensure.That(value).IsGt(0);

            _value = value;
        }

        public static explicit operator Width(int value)
        {
            return new Width(value);
        }

        public static implicit operator int(Width width)
        {
            return width._value;
        }

        private static readonly MemberwiseEqualityComparer<Width> Comparer = MemberwiseEqualityComparer<Width>.ByFields;

        public bool Equals(Width other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Width other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }
    }
}