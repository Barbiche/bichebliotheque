﻿using System;
using System.Runtime.InteropServices;
using EnsureThat;

namespace BarbFoundation.DataStructures
{
    public static class ArrayExtensions
    {
        public static T[,] ToMultidimensional<T>(this T[] @this, Width width, Height height) where T : struct
        {
            EnsureArg.IsNotNull(@this);

            var result = new T[height, width];
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    result[i, j] = @this[i * width + j];
                }
            }

            return result;
        }

        public static unsafe T[] ToSingleDimension<T>(this T[,] @this)
        {
            EnsureArg.IsNotNull(@this);

            var numberOfBytesToCopy = @this.LongLength * Marshal.SizeOf<T>();
            var destination         = new T[@this.Length];

            var sourceHandle      = GCHandle.Alloc(@this, GCHandleType.Pinned);
            var destinationHandle = GCHandle.Alloc(destination, GCHandleType.Pinned);

            try
            {
                var sourcePtr      = sourceHandle.AddrOfPinnedObject();
                var destinationPtr = sourceHandle.AddrOfPinnedObject();

                Buffer.MemoryCopy((void*) sourcePtr, (void*) destinationPtr, numberOfBytesToCopy, numberOfBytesToCopy);
            }
            finally
            {
                sourceHandle.Free();
                destinationHandle.Free();
            }

            return destination;
        }
    }
}