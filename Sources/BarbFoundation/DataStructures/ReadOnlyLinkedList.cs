﻿using System.Collections.Generic;

namespace BarbFoundation.DataStructures
{
    public sealed class ReadOnlyLinkedList<T> : IReadOnlyLinkedList<T>
    {
        private readonly LinkedList<T> _linkedList;

        public ReadOnlyLinkedList(LinkedList<T> linkedList)
        {
            _linkedList = linkedList;
        }

        public LinkedListNode<T> First => _linkedList.First;

        public LinkedListNode<T> Last => _linkedList.Last;

        public Option<LinkedListNode<T>> Get(T value)
        {
            var result = _linkedList.Find(value);
            return result == null ? OptionExtensions.None<LinkedListNode<T>>() : OptionExtensions.Some(result);
        }
    }

    public interface IReadOnlyLinkedList<T>
    {
        LinkedListNode<T>         First { get; }
        LinkedListNode<T>         Last  { get; }
        Option<LinkedListNode<T>> Get(T value);
    }
}