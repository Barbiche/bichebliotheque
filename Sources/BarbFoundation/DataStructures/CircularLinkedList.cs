﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace BarbFoundation.DataStructures
{
    [DebuggerDisplay("Count = {Count}")]
    [Serializable]
    public class CircularLinkedList<T> : ICollection<T>, ICollection, IReadOnlyCollection<T>, ISerializable,
        IDeserializationCallback
    {
        private readonly LinkedList<T> _internalLinkedList;

        public CircularLinkedList()
        {
            _internalLinkedList = new LinkedList<T>();
        }

        public CircularLinkedList(LinkedList<T> internalLinkedList)
        {
            _internalLinkedList = internalLinkedList;
        }

        public CircularLinkedList(IEnumerable<T> collection)
        {
            _internalLinkedList = new LinkedList<T>(collection);
        }

        public CircularLinkedListNode<T> First
        {
            get
            {
                var result = _internalLinkedList.First;
                return result == null ? null : new CircularLinkedListNode<T>(result);
            }
        }

        public CircularLinkedListNode<T> Last
        {
            get
            {
                var result = _internalLinkedList.Last;
                return result == null ? null : new CircularLinkedListNode<T>(result);
            }
        }

        public void CopyTo(Array array, int index)
        {
            _internalLinkedList.CopyTo((T[]) array, index);
        }

        public bool   IsSynchronized => false;
        public object SyncRoot       => this;

        public IEnumerator<T> GetEnumerator()
        {
            return _internalLinkedList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _internalLinkedList.GetEnumerator();
        }

        public void Add(T item)
        {
            _internalLinkedList.AddLast(item);
        }

        public void Clear()
        {
            _internalLinkedList.Clear();
        }

        public bool Contains(T item)
        {
            return _internalLinkedList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _internalLinkedList.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return _internalLinkedList.Remove(item);
        }

        public int  Count      => _internalLinkedList.Count;
        public bool IsReadOnly => false;

        public void OnDeserialization(object sender)
        {
            _internalLinkedList.OnDeserialization(sender);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            _internalLinkedList.GetObjectData(info, context);
        }

        public Option<CircularLinkedListNode<T>> Find(T value)
        {
            var results = _internalLinkedList.Find(value);
            return results == null
                ? OptionExtensions.None<CircularLinkedListNode<T>>()
                : OptionExtensions.Some(new CircularLinkedListNode<T>(results));
        }

        public CircularLinkedListNode<T> AddFirst(T value)
        {
            return new CircularLinkedListNode<T>(_internalLinkedList.AddFirst(value));
        }

        public CircularLinkedListNode<T> AddLast(T value)
        {
            return new CircularLinkedListNode<T>(_internalLinkedList.AddLast(value));
        }
    }

    public sealed class CircularLinkedListNode<T>
    {
        private readonly LinkedListNode<T> _linkedListNode;

        public CircularLinkedListNode(LinkedListNode<T> linkedListNode)
        {
            _linkedListNode = linkedListNode;
        }

        public LinkedList<T> List => _linkedListNode.List;

        public CircularLinkedListNode<T> Next
        {
            get
            {
                var result = _linkedListNode.Next;
                return result == null
                    ? new CircularLinkedListNode<T>(_linkedListNode.List.First)
                    : new CircularLinkedListNode<T>(result);
            }
        }

        public CircularLinkedListNode<T> Previous
        {
            get
            {
                var result = _linkedListNode.Previous;
                return result == null
                    ? new CircularLinkedListNode<T>(_linkedListNode.List.Last)
                    : new CircularLinkedListNode<T>(result);
            }
        }

        public T Value
        {
            get => _linkedListNode.Value;
            set => _linkedListNode.Value = value;
        }
    }
}