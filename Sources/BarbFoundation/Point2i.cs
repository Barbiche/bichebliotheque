﻿using System;
using Equ;

namespace BarbFoundation
{
    public readonly struct Point2I : IEquatable<Point2I>
    {
        public int X { get; }
        public int Y { get; }

        public Point2I(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point2I operator +(Point2I p1, Point2I p2)
        {
            return new Point2I(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point2I operator -(Point2I p1, Point2I p2)
        {
            return new Point2I(p1.X - p2.X, p1.Y - p2.Y);
        }

        public static Point2I operator *(Point2I p1, Point2I p2)
        {
            return new Point2I(p1.X * p2.X, p1.Y * p2.Y);
        }

        public static Point2I operator /(Point2I p1, Point2I p2)
        {
            return new Point2I(p1.X / p2.X, p1.Y / p2.Y);
        }

        public static bool operator ==(Point2I a, Point2I b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Point2I a, Point2I b)
        {
            return !a.Equals(b);
        }

        private static readonly MemberwiseEqualityComparer<Point2I> Comparer =
            MemberwiseEqualityComparer<Point2I>.ByProperties;

        public bool Equals(Point2I other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Point2I other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }
}