﻿using System.IO;
using Dom.Rendering;

namespace Inf.PPMWriter
{
    public class PpmWriter : IPpmWriter
    {
        public PpmWriter(string outputFolder)
        {
            OutputFolder = outputFolder;
        }

        public string OutputFolder { get; }

        private int ResolutionX { get; set; }
        private int ResolutionY { get; set; }

        public string Write(Frame frame, string filename)
        {
            var filePath = Path.Combine(OutputFolder, filename);

            ResolutionX = frame.Width;
            ResolutionY = frame.Height;

            using (var file = new StreamWriter(filePath))
            {
                file.WriteLine(CreateHeader(frame));

                for (var y = 0; y < ResolutionY; y++)
                {
                    for (var x = 0; x < ResolutionX; x++)
                    {
                        file.WriteLine(CreatePixelLine(frame, x, y));
                    }
                }

                file.Close();
            }

            return filePath;
        }

        private string CreateHeader(Frame frame)
        {
            return $"P3\n{frame.Width} {frame.Height}\n255";
        }

        private string CreatePixelLine(Frame frame, int x, int y)
        {
            var pixel = frame.Pixels[x, y];
            return $"{pixel.R} {pixel.G} {pixel.B}";
        }
    }
}