﻿using System;
using Equ;

namespace _3DShapes
{
    public readonly struct Sphere : IEquatable<Sphere>
    {
        public Sphere(float radius)
        {
            Radius = radius;
        }

        private static readonly MemberwiseEqualityComparer<Sphere> Comparer =
            MemberwiseEqualityComparer<Sphere>.ByProperties;

        public float Radius { get; }

        public static bool operator ==(Sphere a, Sphere b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Sphere a, Sphere b)
        {
            return !a.Equals(b);
        }

        public bool Equals(Sphere other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Sphere other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }
    }
}