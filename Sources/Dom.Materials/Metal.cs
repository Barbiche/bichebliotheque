﻿using System.Numerics;
using Equ;

namespace Dom.Materials
{
    public class Metal : MemberwiseEquatable<Metal>
    {
        public Metal(Vector3 albedo, float fuzz)
        {
            Albedo = albedo;
            Fuzz   = fuzz;
        }

        public Vector3 Albedo { get; }

        public float Fuzz { get; }
    }
}