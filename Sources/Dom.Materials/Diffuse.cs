﻿using System.Numerics;
using Equ;

namespace Dom.Materials
{
    public class Diffuse : MemberwiseEquatable<Diffuse>
    {
        public Diffuse(Vector3 albedo)
        {
            Albedo = albedo;
        }

        public Vector3 Albedo { get; }
    }
}