﻿using Equ;

namespace Dom.Materials
{
    public class Dielectric : MemberwiseEquatable<Dielectric>
    {
        public Dielectric(float index)
        {
            Index = index;
        }

        public float Index { get; }
    }
}