﻿namespace EntityComponentSystem
{
    public interface ISystemStore
    {
        void AddSystem(IExecutableSystem executableSystem);
        void RemoveSystem(IExecutableSystem systemToRemove);
    }
}