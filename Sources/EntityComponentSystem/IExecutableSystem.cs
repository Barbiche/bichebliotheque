﻿namespace EntityComponentSystem
{
    public interface IExecutableSystem
    {
        void Execute();
    }
}