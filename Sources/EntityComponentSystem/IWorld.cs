﻿namespace EntityComponentSystem
{
    public interface IWorld
    {
        Entity CreateEntity();
        void   AddComponent(Entity         entity, IComponent component);
        void   AddSystem(IExecutableSystem executableSystem);
    }
}