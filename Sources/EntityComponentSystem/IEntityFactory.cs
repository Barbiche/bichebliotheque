﻿using System;

namespace EntityComponentSystem
{
    public interface IEntityFactory
    {
        IObservable<Entity> CreatedObject { get; }
        Entity              CreateEntity();
    }
}