﻿using EntityComponentSystem.ValueObjects;

namespace EntityComponentSystem
{
    public interface IEntityIdFactory
    {
        EntityId Create();
    }
}