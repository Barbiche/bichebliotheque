﻿using System;
using EntityComponentSystem.ValueObjects;
using Equ;

namespace EntityComponentSystem
{
    public readonly struct Entity : IEquatable<Entity>
    {
        private readonly EntityId _entityId;

        internal Entity(EntityId entityId)
        {
            _entityId = entityId;
        }

        private static readonly MemberwiseEqualityComparer<Entity> Comparer =
            MemberwiseEqualityComparer<Entity>.ByFields;

        public bool Equals(Entity other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Entity other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }
    }
}