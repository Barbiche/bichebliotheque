﻿namespace EntityComponentSystem
{
    public interface ISystemRunner
    {
        void Execute();
    }
}