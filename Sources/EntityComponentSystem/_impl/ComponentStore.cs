﻿using System;
using System.Collections.Generic;
using EnsureThat;

namespace EntityComponentSystem._impl
{
    public sealed class ComponentStore : IComponentStore
    {
        private readonly IDictionary<Entity, ICollection<IComponent>> _components =
            new Dictionary<Entity, ICollection<IComponent>>();

        public ComponentStore(IObservable<Entity> entityCreationStream)
        {
            Ensure.That(entityCreationStream).IsNotNull();

            entityCreationStream.Subscribe(entity => _components.Add(entity, new List<IComponent>()));
        }

        public void AddComponentToEntity(Entity entity, IComponent component)
        {
            Ensure.That(component).IsNotNull();

            _components[entity].Add(component);
        }
    }
}