﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EnsureThat;

namespace EntityComponentSystem._impl
{
    public sealed class EntityFactory : IEntityFactory
    {
        private readonly ISubject<Entity> _createdSubject = new ReplaySubject<Entity>();
        private readonly IEntityIdFactory _entityIdFactory;

        public EntityFactory(IEntityIdFactory entityIdFactory)
        {
            Ensure.That(entityIdFactory).IsNotNull();

            _entityIdFactory = entityIdFactory;
        }

        public IObservable<Entity> CreatedObject => _createdSubject.AsObservable();

        public Entity CreateEntity()
        {
            var entity = new Entity(_entityIdFactory.Create());
            _createdSubject.OnNext(entity);
            return entity;
        }
    }
}