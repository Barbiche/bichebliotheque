﻿using EntityComponentSystem.ValueObjects;

namespace EntityComponentSystem._impl
{
    public sealed class EntityIdFactory : IEntityIdFactory
    {
        private int _currentId;

        public EntityId Create()
        {
            return new EntityId(_currentId++);
        }
    }
}