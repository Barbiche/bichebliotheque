﻿using System;
using System.Collections.Generic;
using EnsureThat;

namespace EntityComponentSystem._impl
{
    public sealed class SystemStore : ISystemStore, ISystemRunner
    {
        private readonly ICollection<IExecutableSystem> _executableSystems = new List<IExecutableSystem>();

        public void Execute()
        {
            foreach (var executableSystem in _executableSystems)
            {
                executableSystem.Execute();
            }
        }

        public void AddSystem(IExecutableSystem executableSystem)
        {
            Ensure.That(executableSystem).IsNotNull();

            _executableSystems.Add(executableSystem);
        }

        public void RemoveSystem(IExecutableSystem systemToRemove)
        {
            Ensure.That(systemToRemove).IsNotNull();

            _executableSystems.Remove(systemToRemove);
        }
    }
}