﻿using System;
using System.Collections.Generic;
using EnsureThat;

namespace EntityComponentSystem._impl
{
    public sealed class EntityStore
    {
        private readonly ISet<Entity> _entities = new HashSet<Entity>();

        public EntityStore(IObservable<Entity> entityCreatedStream)
        {
            Ensure.That(entityCreatedStream).IsNotNull();

            entityCreatedStream.Subscribe(entity => _entities.Add(entity));
        }
    }
}