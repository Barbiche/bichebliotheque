﻿using EnsureThat;

namespace EntityComponentSystem._impl
{
    public sealed class World : IWorld
    {
        private readonly IComponentStore _componentStore;
        private readonly IEntityFactory  _entityFactory;
        private readonly ISystemStore    _systemStore;

        public World(IEntityFactory entityFactory, IComponentStore componentStore, ISystemStore systemStore)
        {
            Ensure.That(entityFactory).IsNotNull();
            Ensure.That(componentStore).IsNotNull();
            Ensure.That(systemStore).IsNotNull();

            _entityFactory  = entityFactory;
            _componentStore = componentStore;
            _systemStore    = systemStore;
        }

        public Entity CreateEntity()
        {
            return _entityFactory.CreateEntity();
        }

        public void AddComponent(Entity entity, IComponent component)
        {
            Ensure.That(component).IsNotNull();

            _componentStore.AddComponentToEntity(entity, component);
        }

        public void AddSystem(IExecutableSystem executableSystem)
        {
            Ensure.That(executableSystem).IsNotNull();

            _systemStore.AddSystem(executableSystem);
        }
    }
}