﻿using System;
using Equ;

namespace EntityComponentSystem.ValueObjects
{
    public readonly struct EntityId : IEquatable<EntityId>
    {
        private readonly int _value;

        public EntityId(int value)
        {
            _value = value;
        }

        public static explicit operator EntityId(int value)
        {
            return new EntityId(value);
        }

        public static implicit operator int(EntityId id)
        {
            return id._value;
        }

        private static readonly MemberwiseEqualityComparer<EntityId> Comparer =
            MemberwiseEqualityComparer<EntityId>.ByFields;

        public bool Equals(EntityId other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is EntityId other && Comparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }

        public override string ToString()
        {
            return $"Id: {_value}";
        }
    }
}