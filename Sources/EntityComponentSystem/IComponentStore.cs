﻿namespace EntityComponentSystem
{
    public interface IComponentStore
    {
        void AddComponentToEntity(Entity entity, IComponent component);
    }
}