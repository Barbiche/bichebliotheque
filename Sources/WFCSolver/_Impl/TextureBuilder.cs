﻿using System.Drawing;
using Dom.Rendering;
using EnsureThat;
using WFCSolver.Application;
using WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public sealed class TextureBuilder : ITextureBuilder
    {
        public ITexture Build(Grid grid)
        {
            Ensure.That(grid).IsNotNull();

            var tileWidth  = grid.Tiles[0, 0].Domain.Height;
            var tileHeight = grid.Tiles[0, 0].Domain.Width;
            var gridWidth  = grid.Tiles.GetLength(0) * tileWidth;
            var gridHeight = grid.Tiles.GetLength(1) * tileHeight;

            var colors = new Color[gridWidth, gridHeight];

            for (var i = 0; i < grid.Tiles.GetLength(0); i++)
            {
                for (var j = 0; j < grid.Tiles.GetLength(1); j++)
                {
                    var tile = grid.Tiles[i, j];
                    var pattern = tile.Pattern.Proj(() =>
                        new Pattern(new Texture(new Color[tileWidth, tileHeight])));
                    for (var k = 0; k < pattern.Texture.Width; k++)
                    {
                        for (var l = 0; l < pattern.Texture.Height; l++)
                        {
                            colors[i * tileWidth + k, j * tileHeight + l] = pattern.Texture[new PixelCoordinate(k, l)];
                        }
                    }
                }
            }

            return new Texture(colors);
        }
    }
}