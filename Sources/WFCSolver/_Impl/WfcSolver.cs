﻿using System.Drawing;
using System.Linq;
using BarbFoundation;
using EnsureThat;
using Functional;
using WFCSolver.Application;
using WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public class WfcSolver : IWfcSolver
    {
        private readonly IGridFactory      _gridFactory;
        private readonly IPatternExtractor _patternExtractor;
        private readonly ISampleFactory    _sampleFactory;

        public WfcSolver(ISampleFactory sampleFactory, IGridFactory gridFactory, IPatternExtractor patternExtractor)
        {
            Ensure.That(sampleFactory, nameof(sampleFactory)).IsNotNull();
            Ensure.That(gridFactory, nameof(gridFactory)).IsNotNull();
            Ensure.That(patternExtractor, nameof(patternExtractor)).IsNotNull();

            _sampleFactory    = sampleFactory;
            _gridFactory      = gridFactory;
            _patternExtractor = patternExtractor;
        }

        public Option<Color[,]> Solve(string samplePath, int width, int height)
        {
            Ensure.That(samplePath, nameof(samplePath)).IsNotNull();

            // Load sample
            var sample = _sampleFactory.Create(samplePath);

            // Generate patterns
            var patterns = _patternExtractor.ExtractPatterns(sample, new SizeParameter(2));
            var initialGrid =
                _gridFactory.Create(new Width(width), new Height(height), patterns.Cast<IPattern>().ToArray());

            foreach (var tile in initialGrid.Tiles)
            {
                tile.Domain.ApplyRandomPattern();
            }

            var textureBuilder = new TextureBuilder();
            textureBuilder.Build(initialGrid);

            return OptionExtensions.Some(textureBuilder.Build(initialGrid).Pixels);
        }
    }
}