﻿using System;
using BarbFoundation.DataStructures;
using EnsureThat;
using IO.Images;
using WFCSolver.Application;
using WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public sealed class SampleFactory : ISampleFactory
    {
        private readonly IImageLoader _imageLoader;

        public SampleFactory(IImageLoader imageLoader)
        {
            Ensure.That(imageLoader).IsNotNull();

            _imageLoader = imageLoader;
        }

        public Sample Create(string imagePath)
        {
            return _imageLoader.Load(imagePath)
                .Match(
                    image => new Sample(
                        new Texture(image.Pixels.ToArray().ToMultidimensional(image.Width, image.Height))),
                    () => throw new Exception($"No image has been created from path {imagePath}"));
        }
    }
}