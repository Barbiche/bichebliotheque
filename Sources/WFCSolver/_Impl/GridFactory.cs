﻿using BarbFoundation;
using EnsureThat;
using WFCSolver.Application;
using WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public class GridFactory : IGridFactory
    {
        private readonly IDomainFactory _domainFactory;

        public GridFactory(IDomainFactory domainFactory)
        {
            Ensure.That(domainFactory, nameof(domainFactory)).IsNotNull();

            _domainFactory = domainFactory;
        }

        public Grid Create(Width width, Height height, IPattern[] initialDomain)
        {
            Ensure.That(initialDomain).IsNotNull();

            return new Grid(CreateTilesGrid(width, height, initialDomain));
        }

        private Tile[,] CreateTilesGrid(Width width, Height height, IPattern[] initialDomain)
        {
            var tiles = new Tile[height, width];
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    tiles[i, j] = new Tile(_domainFactory.Create(initialDomain));
                }
            }

            return tiles;
        }
    }
}