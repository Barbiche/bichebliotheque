﻿using System;
using System.Collections.Generic;
using System.Linq;
using BarbFoundation;
using EnsureThat;
using Functional;
using WFCSolver.Application;
using WFCSolver.Domain;
using WFCVisualizer.Sources.WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public class UniformDomain : IWeightedDomain
    {
        private readonly Random                       _random;
        private readonly Dictionary<IPattern, Weight> _weightedDomain;

        public UniformDomain(IPattern[] patterns, Random random)
        {
            Ensure.That(patterns).IsNotNull();
            Ensure.That(patterns.GroupBy(pattern => pattern.Texture.Width).Count()).Is(1);
            Ensure.That(patterns.GroupBy(pattern => pattern.Texture.Height).Count()).Is(1);
            Ensure.That(random).IsNotNull();

            _random         = random;
            _weightedDomain = new Dictionary<IPattern, Weight>();
            foreach (var pattern in patterns)
            {
                if (_weightedDomain.ContainsKey(pattern))
                {
                    _weightedDomain[pattern]++;
                }
                else
                {
                    _weightedDomain.Add(pattern, 1);
                }
            }

            Height = (Height) patterns[0].Texture.Height;
            Width  = (Width) patterns[0].Texture.Width;
        }

        public Height           Height         { get; }
        public Width            Width          { get; }
        public int              Entropy        => _weightedDomain.Count - 1;
        public Option<IPattern> ChoosenPattern { get; private set; }

        public void ApplyRandomPattern()
        {
            var absolutePatterns = new List<IPattern>();
            foreach (var pair in _weightedDomain)
            {
                for (var i = 0; i < pair.Value; i++)
                {
                    absolutePatterns.Add(pair.Key);
                }
            }

            var absArray = absolutePatterns.ToArray();
            ChoosenPattern = OptionExtensions.Some(absArray[_random.Next(0, absArray.Length)]);
        }
    }
}