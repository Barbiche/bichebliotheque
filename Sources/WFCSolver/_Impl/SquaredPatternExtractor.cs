﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using EnsureThat;
using WFCSolver.Application;
using WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public class SquaredPatternExtractor : IPatternExtractor
    {
        public SquaredPattern[] ExtractPatterns(Sample sample, SizeParameter sizeParameter)
        {
            Ensure.That(sample).IsNotNull();
            Ensure.That(sizeParameter).IsNotNull();

            Ensure.That((int) sizeParameter).IsLte(sample.Texture.Width);
            Ensure.That((int) sizeParameter).IsLte(sample.Texture.Height);

            var patterns = new HashSet<SquaredPattern>();
            for (var i = 0; i < sample.Texture.Width; i += sizeParameter)
            {
                for (var j = 0; j < sample.Texture.Height; j += sizeParameter)
                {
                    var pixels = new Color[sizeParameter, sizeParameter];
                    for (var k1 = 0; k1 < sizeParameter; k1++)
                    {
                        for (var k2 = 0; k2 < sizeParameter; k2++)
                        {
                            pixels[k1, k2] = sample.Texture.Pixels[i + k1, j + k2];
                        }
                    }

                    patterns.Add(new SquaredPattern(new SquaredTexture(pixels)));
                }
            }

            return patterns.ToArray();
        }
    }
}