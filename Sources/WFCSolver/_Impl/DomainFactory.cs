﻿using System;
using WFCSolver.Application;
using WFCSolver.Domain;

namespace WFCSolver._Impl
{
    public sealed class DomainFactory : IDomainFactory
    {
        private readonly Random _random = new Random();

        public IWeightedDomain Create(IPattern[] patterns)
        {
            return new UniformDomain(patterns, _random);
        }
    }
}