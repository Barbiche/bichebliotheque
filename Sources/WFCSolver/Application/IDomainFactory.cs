﻿using WFCSolver.Domain;

namespace WFCSolver.Application
{
    public interface IDomainFactory
    {
        IWeightedDomain Create(IPattern[] patterns);
    }
}