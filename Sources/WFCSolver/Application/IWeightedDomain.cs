﻿using BarbFoundation;
using Functional;
using WFCSolver.Domain;

namespace WFCSolver.Application
{
    public interface IWeightedDomain
    {
        Height           Height         { get; }
        Width            Width          { get; }
        int              Entropy        { get; }
        Option<IPattern> ChoosenPattern { get; }
        void             ApplyRandomPattern();
    }
}