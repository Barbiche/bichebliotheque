﻿using WFCSolver.Domain;

namespace WFCSolver.Application
{
    public interface IPatternExtractor
    {
        SquaredPattern[] ExtractPatterns(Sample sample, SizeParameter sizeParameter);
    }
}