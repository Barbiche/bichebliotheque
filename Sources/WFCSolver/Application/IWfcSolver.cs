﻿using System.Drawing;
using Functional;

namespace WFCSolver.Application
{
    public interface IWfcSolver
    {
        Option<Color[,]> Solve(string samplePath, int width, int height);
    }
}