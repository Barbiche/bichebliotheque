﻿using BarbFoundation;
using WFCSolver.Domain;

namespace WFCSolver.Application
{
    public interface IGridFactory
    {
        Grid Create(Width width, Height height, IPattern[] initialDomain);
    }
}