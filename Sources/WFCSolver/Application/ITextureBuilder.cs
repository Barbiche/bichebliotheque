﻿using System;
using System.Collections.Generic;
using System.Text;
using WFCSolver.Domain;

namespace WFCSolver.Application
{
    public interface ITextureBuilder
    {
        ITexture Build(Grid grid);
    }
}
