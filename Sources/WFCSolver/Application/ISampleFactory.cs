﻿using WFCSolver.Domain;

namespace WFCSolver.Application
{
    public interface ISampleFactory
    {
        Sample Create(string imagePath);
    }
}