﻿using System.Linq;
using EnsureThat;
using Equ;
using Functional;

namespace WFCSolver.Domain
{
    public sealed class Grid : MemberwiseEquatable<Grid>
    {
        public Grid(Tile[,] tiles)
        {
            Ensure.That(tiles).IsNotNull();

            Tiles = tiles;
        }

        public Tile[,] Tiles { get; }

        public bool AreAllTilesSolved()
        {
            return Tiles.Cast<Tile>().All(tile => tile.Domain.Entropy == 0);
        }

        public Option<Tile> PickLeastEntropyTile()
        {
            var  entropyTamp = int.MaxValue;
            Tile tileTamp    = null;
            foreach (var tile in Tiles)
            {
                if (tile.Domain.Entropy < entropyTamp && tile.Domain.Entropy != 0)
                {
                    tileTamp    = tile;
                    entropyTamp = tile.Domain.Entropy;
                }
            }

            return tileTamp == null ? OptionExtensions.None<Tile>() : OptionExtensions.Some(tileTamp);
        }
    }
}