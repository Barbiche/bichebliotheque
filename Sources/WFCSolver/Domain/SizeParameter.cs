﻿using EnsureThat;
using Equ;

namespace WFCSolver.Domain
{
    public sealed class SizeParameter : MemberwiseEquatable<SizeParameter>
    {
        private readonly int _value;

        public SizeParameter(int value)
        {
            Ensure.That(value).IsGt(0);

            _value = value;
        }

        public static explicit operator SizeParameter(int value)
        {
            return new SizeParameter(value);
        }

        public static implicit operator int(SizeParameter sizeParameter)
        {
            return sizeParameter._value;
        }
    }
}