﻿namespace WFCSolver.Domain
{
    public interface IPattern
    {
        ITexture Texture { get; }
    }
}