﻿using EnsureThat;
using Equ;

namespace WFCSolver.Domain
{
    public sealed class SquaredPattern : MemberwiseEquatable<SquaredPattern>, IPattern
    {
        public SquaredPattern(SquaredTexture texture)
        {
            Ensure.That(texture).IsNotNull();

            Texture       = texture;
            SizeParameter = new SizeParameter(texture.Width);
        }

        public SizeParameter SizeParameter { get; }
        public ITexture      Texture       { get; }
    }
}