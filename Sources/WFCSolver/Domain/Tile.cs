﻿using EnsureThat;
using Equ;
using Functional;
using WFCSolver.Application;

namespace WFCSolver.Domain
{
    public sealed class Tile : MemberwiseEquatable<Tile>
    {
        public Tile(IWeightedDomain domain)
        {
            Ensure.That(domain).IsNotNull();

            Domain = domain;
        }

        public IWeightedDomain  Domain  { get; }
        public Option<IPattern> Pattern => Domain.ChoosenPattern;
    }
}