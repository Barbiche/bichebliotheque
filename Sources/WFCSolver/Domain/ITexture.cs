﻿using System.Drawing;
using Dom.Rendering;

namespace WFCSolver.Domain
{
    public interface ITexture
    {
        Color[,] Pixels { get; }
        int      Width  { get; }
        int      Height { get; }
        Color this[PixelCoordinate coordinate] { get; }
    }
}