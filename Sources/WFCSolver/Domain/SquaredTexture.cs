﻿using System.Drawing;
using Dom.Rendering;
using EnsureThat;
using Equ;

namespace WFCSolver.Domain
{
    public sealed class SquaredTexture : MemberwiseEquatable<SquaredTexture>, ITexture
    {
        public SquaredTexture(Color[,] pixels)
        {
            Ensure.That(pixels.GetLength(0)).IsGt(0);
            Ensure.That(pixels.GetLength(1)).IsGt(0);
            Ensure.That(pixels.GetLength(0)).Is(pixels.GetLength(1));

            Pixels = pixels;
        }

        public Color[,] Pixels { get; }
        public int      Width  => Pixels.GetLength(0);
        public int      Height => Pixels.GetLength(1);
        public Color this[PixelCoordinate coordinate] => Pixels[coordinate.X, coordinate.Y];
    }
}