﻿using EnsureThat;
using Equ;

namespace WFCSolver.Domain
{
    public sealed class Sample : MemberwiseEquatable<Sample>
    {
        public Sample(Texture texture)
        {
            Ensure.That(texture).IsNotNull();

            Texture = texture;
        }

        public Texture Texture { get; }
    }
}