﻿using EnsureThat;
using Equ;

namespace WFCVisualizer.Sources.WFCSolver.Domain
{
    public sealed class Weight : MemberwiseEquatable<Weight>
    {
        private readonly int _value;

        public Weight(int value)
        {
            Ensure.That(value).IsGte(0);
            _value = value;
        }


        public static implicit operator int(Weight weight)
        {
            return weight._value;
        }

        public static implicit operator Weight(int value)
        {
            return new Weight(value);
        }
    }
}