﻿using EnsureThat;
using Equ;

namespace WFCSolver.Domain
{
    public sealed class Pattern : MemberwiseEquatable<Pattern>, IPattern
    {
        public Pattern(Texture texture)
        {
            Ensure.That(texture).IsNotNull();

            Texture = texture;
        }

        public ITexture Texture { get; }
    }
}