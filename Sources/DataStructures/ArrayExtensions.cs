﻿using System;
using System.Runtime.InteropServices;
using EnsureThat;

namespace DataStructures
{
    public static class ArrayExtensions
    {
        public static T[] GetRow<T>(this T[,] array, int row)
        {
            Ensure.That(array).IsNotNull();
            Ensure.That(typeof(T).IsPrimitive).IsTrue();

            var cols   = array.GetUpperBound(1) + 1;
            var result = new T[cols];

            int size;

            if (typeof(T) == typeof(bool))
            {
                size = 1;
            }
            else if (typeof(T) == typeof(char))
            {
                size = 2;
            }
            else
            {
                size = Marshal.SizeOf<T>();
            }

            Buffer.BlockCopy(array, row * cols * size, result, 0, cols * size);

            return result;
        }
    }
}