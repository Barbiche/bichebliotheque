﻿using System;

namespace Exhibitors
{
    public interface ICreatedExhibitor<out T>
    {
        IObservable<T> CreatedObject { get; }
    }
}