﻿using System;
using Equ;

namespace Dom.Meshes
{
    public sealed class VertexBufferIdentity : MemberwiseEquatable<VertexBufferIdentity>
    {
        private readonly Guid _guid;

        public VertexBufferIdentity(Guid guid)
        {
            _guid = guid;
        }

        public VertexBufferIdentity() : this(Guid.NewGuid()) { }

        public static implicit operator Guid(VertexBufferIdentity vbi)
        {
            return vbi._guid;
        }

        public static implicit operator VertexBufferIdentity(Guid guid)
        {
            return new VertexBufferIdentity(guid);
        }
    }
}