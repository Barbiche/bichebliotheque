﻿using System;
using Equ;

namespace Dom.Meshes
{
    public readonly struct Index : IEquatable<Index>
    {
        private readonly        int                               _value;
        private static readonly MemberwiseEqualityComparer<Index> Comparer = MemberwiseEqualityComparer<Index>.ByFields;

        public Index(int value)
        {
            _value = value;
        }

        public static implicit operator int(Index index)
        {
            return index._value;
        }

        public static implicit operator Index(int value)
        {
            return new Index(value);
        }

        public bool Equals(Index other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Index other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }
    }
}