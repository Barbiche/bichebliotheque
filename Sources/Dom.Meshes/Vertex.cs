﻿using System;
using System.Numerics;
using Equ;

namespace Dom.Meshes
{
    public readonly struct Vertex : IEquatable<Vertex>
    {
        private static readonly MemberwiseEqualityComparer<Vertex> Comparer =
            MemberwiseEqualityComparer<Vertex>.ByFields;

        private readonly Vector3 _value;

        public Vertex(Vector3 value)
        {
            _value = value;
        }

        public bool Equals(Vertex other)
        {
            return Comparer.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            return obj is Vertex other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Comparer.GetHashCode(this);
        }

        public static implicit operator Vector3(Vertex vertex)
        {
            return vertex._value;
        }

        public static implicit operator Vertex(Vector3 value)
        {
            return new Vertex(value);
        }
    }
}