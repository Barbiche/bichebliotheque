﻿using System;

namespace Dom.Meshes
{
    public interface IVerticesRepository
    {
        ReadOnlyMemory<Vertex> Get(VertexBufferIdentity   identity);
        void                   Set(VertexBufferIdentity   identity, ReadOnlyMemory<Vertex> vertices);
        VertexBufferIdentity   Set(ReadOnlyMemory<Vertex> vertices);
    }
}