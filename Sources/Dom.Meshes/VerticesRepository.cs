﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Dom.Meshes
{
    public sealed class VerticesRepository : IVerticesRepository
    {
        private readonly IDictionary<VertexBufferIdentity, ReadOnlyMemory<Vertex>> _vertices =
            new ConcurrentDictionary<VertexBufferIdentity, ReadOnlyMemory<Vertex>>();

        public ReadOnlyMemory<Vertex> Get(VertexBufferIdentity identity)
        {
            if (!_vertices.ContainsKey(identity))
            {
                throw new ArgumentException($"{identity} is not in repository.");
            }

            return _vertices[identity];
        }

        public void Set(VertexBufferIdentity identity, ReadOnlyMemory<Vertex> vertices)
        {
            if (!_vertices.ContainsKey(identity))
            {
                throw new ArgumentException($"{identity} is already set in repository.");
            }

            _vertices.Add(identity, vertices);
        }

        public VertexBufferIdentity Set(ReadOnlyMemory<Vertex> vertices)
        {
            var id = new VertexBufferIdentity();
            _vertices.Add(id, vertices);
            return id;
        }
    }
}