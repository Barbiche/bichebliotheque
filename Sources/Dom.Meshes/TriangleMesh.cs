﻿using System;
using Equ;

namespace Dom.Meshes
{
    public sealed class TriangleMesh : MemberwiseEquatable<TriangleMesh>
    {
        public ReadOnlyMemory<Index> Indices;
        public VertexBufferIdentity  VertexBufferIdentity;

        public TriangleMesh(VertexBufferIdentity vertexBufferIdentity, ReadOnlyMemory<Index> indices)
        {
            VertexBufferIdentity = vertexBufferIdentity;
            Indices              = indices;
        }
    }
}