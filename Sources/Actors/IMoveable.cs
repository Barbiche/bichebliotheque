﻿namespace Actors
{
    public interface IMoveable
    {
        void MoveX(float amount);
    }
}